/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas.akhir.jarkom;
import java.net.*;
import java.io.*;
import java.util.*;
/**
 *
 * @author Amen
 */
public class Server {
    static Vector<Players> ar = new Vector<>();
    static int i = 0;
            
    public static void main(String[] args) throws IOException {
        System.out.println("Server Start");
        ServerSocket serversocket = new ServerSocket(5000);
        Socket socket;
        while(true){
        socket = serversocket.accept();
        System.out.println(socket.getInetAddress() + "Connected");
        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
        DataInputStream in = new DataInputStream(socket.getInputStream());
        Players th = new Players(socket,"Player connected" + i, in, out);
        Thread thread = new Thread(th);
        ar.add(th);
        thread.start();
        i++;
        }
        }
}

class Players implements Runnable{
    
    Scanner scan = new Scanner(System.in);
    final DataOutputStream out;
    final DataInputStream in;
    private String name;
    Socket socket;
    boolean login;
    private String string;
    public Players(Socket socket, String name, DataOutputStream out, DataInputStream in){
        this.out = out;
        this.in = in;
        this.name = name;
        this.socket = socket;
        this.login=true;
    }

    public Players(Socket socket, String string, DataInputStream in, DataOutputStream out) {
        this.socket = socket;
        this. string = string;
        this.out = out;
        this.in = in;
    }

    public void run() {
        
        String received;
        int bosshp = 1000;
        while(true){
            try {
                received = in.readUTF();
                StringTokenizer st = new StringTokenizer(received, "#");
                received = in.readUTF();
                System.out.println(received);
                
                if(received.equals("hit")){
                bosshp -= 10;
                System.out.println("Hit Received " + bosshp);
                } else if (received.equals("miss")) {
                    System.out.println("Hit Missed");
                }
                
                if (bosshp == 0){
                   this.login=false;
                   this.socket.close();
                   break;
                }
                
                
            } catch (IOException e) {
            }
        }
        try {
        this.in.close();
        this.out.close();
        } catch (IOException e){
        }
    }
}
